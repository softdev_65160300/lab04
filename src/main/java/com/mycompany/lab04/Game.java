/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class Game {
    private Table table;
    private Player player1,player2;

    public Game() {
        player1 = new Player("O");
        player2 = new Player("X");
    }
    public void play(){
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                showCurrentPlayer();
                newGame();
                if(!Continue()){
                    break;
                }
            }
            if(table.checkDraw()){
                newGame();
                printDraw();
                if(!Continue()){
                    break;
                }
            }
            table.switchPlayer();
        }
    }

    private void showWelcome() {
       System.out.println("Welcome to OX Game");    
    }

    private void showTable() {
        String[][] t = table.getTable();
        for(int i = 0;i<3;i++){
            for(int j =0;j<3;j++){
                System.out.print(t[i][j]+ " ");
            }
            System.out.println();
        }
    }

    public void newGame() {
        this.table = new Table(player1,player2);
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.print("Please input row col: ");
        int row = kb.nextInt();
        int col = kb.nextInt();
        table.setRowCol(row, col);
        
        }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    private void showCurrentPlayer() {
        System.out.println(player1);
        System.out.println(player2);
    }

    private void printDraw() {
        System.out.println("Draw");
    }
    private boolean Continue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Continue(y/n):");
        String yn = kb.next();
        return yn.equalsIgnoreCase("y");
    }
    
}
