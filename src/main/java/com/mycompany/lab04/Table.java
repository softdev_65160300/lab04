/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author Acer
 */
public class Table {
    private String[][] table = {{"-","-","-"},{"-","-","-"},{"-","-","-"}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int turnCount = 0;
    private int row, col;
    
    public Table(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }
    public String[][] getTable(){
        return table;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public boolean setRowCol(int row, int col){
        if(table[row][col] == "-"){
            table[row][col] = currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            return true;
        }
        return false;
    }
    
    public boolean checkWin(){
        if(checkRow()){
            saveWin();
            return true;
        }
        if(checkCol()){
            saveWin();
            return true;
        }
        if(checkX1()){
            saveWin();
            return true;
        }
        if(checkX2()){
            saveWin();
            return true;
        }
        return false;
    }
    private boolean checkRow(){
        String symbol = currentPlayer.getSymbol();
        for(int i = 0;i<3;i++){
            if(table[row][i] != symbol){
                return false;
            }
        }
        return true;
    }
    private boolean checkCol(){
        String symbol = currentPlayer.getSymbol();
        for(int j = 0;j<3;j++){
            if(table[j][col] != symbol){
                return false;
            }
        }
        return true;
    }
    private boolean checkX1(){
        String symbol = currentPlayer.getSymbol();
        for(int i = 0;i<3;i++){
            if(table[i][i] != symbol){
                return false;
            }
        }
        return true;
    }
    private boolean checkX2(){
        String symbol = currentPlayer.getSymbol();
        for(int i = 0;i<3;i++){
            if(table[i][2-i] != symbol){
                return false;
            }
        }
        return true;
    }
    void switchPlayer() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }  
    private void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else{
            player1.lose();
            player2.win();
        }  
    }
    private void saveDraw(){
        player1.draw();
        player2.draw();
    }

    boolean checkDraw() {
        for (int i = 0; i<3;i++){
            for(int j = 0;j<3;j++){
                if (table[i][j] == "-"){
                    return false;
                }
            }
        }
        return true;
    }
}
